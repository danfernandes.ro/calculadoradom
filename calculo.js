var notas = document.getElementById("form_nota");
var calculo = document.getElementById("form_envio");
var lista_notas = new Array();
var soma = parseFloat(0);
var media = parseFloat(0);

/*function lista(){
    //recebendo a nota
    var nota_atual = parseFloat(document.getElementById("inputnota").value);   
    console.log(nota_atual);
    
    //criando array
    lista_notas.push(nota_atual);
    console.log(lista_notas);
}*/

function mostrarnotas(){
    console.log(notas);

    //recebendo a nota
    var nota_atual = parseFloat(document.getElementById("inputnota").value);   
    console.log(nota_atual);
    
    //criando array
    lista_notas.push(nota_atual);
    console.log(lista_notas);


    let paragrafo = document.createElement("p");
    let input = document.querySelector(".inputnota");
    paragrafo.innerText = input;
    
    if (nota_atual.trim==="" || nota_atual<0 || isNaN(paragrafo.innerText)==true || isNaN(nota_atual)==true){
        return alert ("A nota digitada é inválida. Digite valores válidos!");
    }

    if(nota_atual>10){
        return alert ("A nota máxima é 10!");
    }
    
    else{
        let novohistorico = document.querySelector(".historico");
        let div = document.createElement("div");
        novohistorico.append(div);
        div.append(paragrafo);
        paragrafo.innerText = 'A nota foi: ' + nota_atual + '.';
    }
}

function calcularMedia(){
    for(var i = 0; i< lista_notas.length; i++){
        var numero = lista_notas[i];
        soma += numero;
    }

    media = (soma / lista_notas.length)
    console.log(media.toFixed(2));
    let fim = document.querySelector(".resultadofinal");
    fim.innerText='A média foi: ' + media.toFixed(2) +'.';
}


//botao enviar
let add = document.querySelector(".botao")
add.addEventListener("click", ()=>mostrarnotas());

//botao calcular
let calcular = document.querySelector(".concluir")
calcular.addEventListener("click",()=>calcularMedia());
